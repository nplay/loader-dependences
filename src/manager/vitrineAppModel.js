
import APIManager from './api/apiManager'
import CacheHelper from '../helper/cache'

import ClientCollection from './clientCollection'

import RequestHelper from './../helper/Http/Request'

class VitrineAppModel
{
    constructor()
    {
        this.SERVICE = {
            history: {
                URL: "//history.service.nplay.com.br/",
                insert: {
                    URL: "//history.service.nplay.com.br/v1/insert",
                    method: 'POST'
                },
                get: {
                    URL: "//history.service.nplay.com.br/v1/get",
                    method: 'GET'
                },
                weather: {
                    byIP: {
                        URL: "//history.service.nplay.com.br/v1/weather/byIP",
                        method: 'POST'
                    }
                }
            },
            api: {
                analytics: {
                    insert: {
                        URL: "//api.service.nplay.com.br/v1/analytics/insert"
                    }
                }
            }
        }

        this.API = {
            URL: "https://api.nplay.com.br/",
            user: {
                URL: "user/",
                method: "GET"
            },
            searches: {
                URL: "searches/",
                item: {
                    URL: "item/",
                    buyToBuy: {
                        URL: "buy-to-buy",
                        method: "POST"
                    },
                    bestSeller: {
                        URL: "best-seller/",
                        categorys: {
                            URL: "categorys/",
                            method: "POST"
                        }
                    },
                    new: {
                        URL: "new/",
                        method: "GET",
                        categorys: {
                            URL: "categorys/",
                            method: "POST"
                        }
                    },
                    promotional: {
                        URL: "promotional/",
                        categorys: {
                            URL: "categorys/",
                            method: "POST"
                        }
                    },
                    category: {
                        URL: 'category',
                        method: 'POST'
                    }
                },
                history: {
                    URL: "history/",
                    item: {
                        URL: "item/",
                        method: "POST"
                    }
                }
            },
            themes: {
                URL: "themes/",
                popup: {
                    URL: "popup/",
                    method: "POST"
                }
            }
        }

        this.CDN = {
            URL: "https://cdn.nplay.com.br/",
            URL_VITRINE: "vitrine/"
        }
        
        this.clients = new ClientCollection

        this.publicClasses = this._loadPublicClasses()
    }

    async ip()
    {
        let cache = new CacheHelper('vitrineApp.ip')
        
        try {
            if(cache.hasCached())
                return cache.getCache()

            let response = await fetch('//api.ipify.org')

            if(response.status != 200)
                throw new Error(`Status code invalid`)

            let ip = await response.text()
            cache.setCache(ip)

        } catch (error) {
            console.warn(`Fail retrive external IP ${error.message}`)
        }
        finally{
            return cache.getCache()
        }
    }

    _loadPublicClasses()
    {
        return {
            loader: {
                RequestHelper,
                CacheHelper,
                APIManager
            }
        }
    }
}

export default VitrineAppModel