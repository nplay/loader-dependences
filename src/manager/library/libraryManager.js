import RenderLibrary from "./render";
import AnalyticsLibrary from "./analytics";
import PopupLibrary from "./popup";

import Dependence from "../dependence";

class LibraryManager
{
    constructor(token)
    {
        this.token = token
        this.render = new RenderLibrary(this.token)
        this.analytics = new AnalyticsLibrary(this.token)
        this.popup = new PopupLibrary(this.token);
    }

    async loads(librarys, options = {})
    {
        for(let name of librarys){
            let library = await this.load(name, options)
            library.run()
        }
    }

    async load(libraryName, options = {})
    {
        if(!this.hasOwnProperty(libraryName))
            throw new Error(`Library ${libraryName} not found in LibraryManager`)

        let option = options[libraryName] || null

        let url = await this[libraryName].getSource()
        await Dependence.load(url, this.token, option)

        return this[libraryName]
    }
}

export default LibraryManager