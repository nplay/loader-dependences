import AbstractLibrary from '../abstractLibrary'
import LibraryManager from '../libraryManager';

class RenderLibrary extends AbstractLibrary
{
    constructor(token)
    {
        super(token)
    }

    async getSource()
    {
        let client = window.vitrineApp.clients.get(this.token)
            
        let urlVitrine = client.CDN_URL_VITRINE
        let url = `${urlVitrine}render/js/bundle.js`
        
        return url
    }

    /**
     * Renderize vitrine
     */
    async run()
    {
        let libraryMan = new LibraryManager(this.token)
        await libraryMan.load('render')

        let library = null
        library = await libraryMan.load('analytics')
        await library.run()

        let client = window.vitrineApp.clients.get(this.token)

        let render = client.publicLibraries.render
        let renders = await render.getCollection()
        client.user.data.vitrineCollection = renders

        for(let render of client.user.data.vitrineCollection)
            render.mountVueComponent()
    }
}

export default RenderLibrary