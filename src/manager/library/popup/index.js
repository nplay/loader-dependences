import AbstractLibrary from '../abstractLibrary'
import LibraryManager from '../libraryManager';

class PopupLibrary extends AbstractLibrary
{
    constructor(token)
    {
        super(token)
    }

    async initialize()
    {
        let libraryMan = new LibraryManager(this.token)
        
        await libraryMan.load('analytics').run()
        await libraryMan.load('popup').run()
    }

    async getSource()
    {
        let client = window.vitrineApp.clients.get(this.token)
            
        let urlVitrine = client.CDN_URL_VITRINE
        let url = `${urlVitrine}popup/js/bundle.js`
        return url
    }
}

export default PopupLibrary