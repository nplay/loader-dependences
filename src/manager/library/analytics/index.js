import AbstractLibrary from '../abstractLibrary'
import LibraryManager from '../libraryManager';

class AnalyticsLibrary extends AbstractLibrary
{
    constructor(token)
    {
        super(token)
    }

    async initialize()
    {
        let libraryMan = new LibraryManager(this.token)
        libraryMan.load('analytics')
    }

    async getSource()
    {
        try {
            let client = window.vitrineApp.clients.get(this.token)
            
            let urlVitrine = client.CDN_URL_VITRINE
            let url = `${urlVitrine}analytics/${client.info.platform}.js`
            
            return url

        } catch (error) {
            throw new Error(`User not found by token plugin not initialized`)
        }
    }
}

export default AnalyticsLibrary