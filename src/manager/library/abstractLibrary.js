
class AbstractLibrary
{
    constructor(token)
    {
        this.token = token
    }

    async getSource()
    {
        throw new Error('Implement method getSource in child class')
    }

    async run()
    {
        
    }
}

export default AbstractLibrary