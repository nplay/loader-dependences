import DocumentCollection from "../helper/documentCollection";
import ClientModel from './clientModel'

class ClientCollection extends DocumentCollection
{
    constructor()
    {
        super(ClientModel)
    }

    loadClient(token, options = null)
    {
        let clientModel = new ClientModel(token)
        let tokenData = window.atob(token.split('.')[1])
        let tokenInfo = JSON.parse(tokenData)

        clientModel.info.version = this._getClientVersion(token) || clientModel.info.version
        clientModel.info = Object.assign(clientModel.info, tokenInfo)
        
        this.push(token, clientModel)

        let libs = options || {}
        libs = libs.librarys || ['analytics', 'render', 'popup']
        clientModel.library.loads(libs, options)
    }

    _getClientVersion(token)
    {
        let clientsVersion = new Object({
            //'1557431868246': '1.0.0'
        })

        return clientsVersion.hasOwnProperty(token) ? clientsVersion[token] : null
    }
}

export default ClientCollection