import axios from 'axios'

class Dependence
{
    static async load(url, token, options = null)
    {
        let key = Dependence.pluginKey(url, token)

        if(Dependence.canLoaded(key))
            return true;

        let response = await Dependence.download(url)
        Dependence._loadedPlugins[key] = response.data

        let plugin = Dependence._loadedPlugins[key]

        options = JSON.stringify(options)
        let code = `var token = '${token}'; var options = '${options}'; ${plugin}`

        let vm = require('vm');
        let script = new vm.Script(code, { filename: 'myfile.vm' });
        script.runInThisContext();

        return true
    }

    static async download(url)
    {
        return await axios.get(url)
    }

    static pluginKey(url, token)
    {
        return `${url}_${token}`
    }

    static canLoaded(key)
    {
        return Dependence._loadedPlugins.hasOwnProperty(key)
    }
}

if(Dependence._loadedPlugins == undefined)
    Dependence._loadedPlugins = new Object

export default Dependence