import Weather from "./data/weather"
import HistoryAPI from '../../api/history/indexAPI'

class DataModel
{
    constructor(token)
    {
        this.token = token

        this.weather = new Weather(this.token)

        this.vitrineCollection = new Array()
    }

    async _historyData()
    {
        let client = window.vitrineApp.clients.get(this.token)
        let sessionID = await client.user.sessionID

        let data = {
            sessionID: sessionID
        }

        let vitrineUser = window.vitrineMeta.user
        if(vitrineUser)
            data.user = vitrineUser

        let histAPI = new HistoryAPI(this.token, false)
        let response = await histAPI.update(data)

        if(response.success == false)
            throw new Error(response.message)

        return response.data.data.user
    }

    async get()
    {
        if(!this._data)
            this._data = await this._historyData()

        return this._data
    }

    async email()
    {
        let userData = await this.get()
        return userData == null ? null : userData.email
    }

    async name()
    {
        let userData = await this.get()
        return userData == null ? null : userData.name
    }
}

export default DataModel