
import UserDataModel from './data'
import Cache from '../../../helper/cache'
import HistoryAPI from '../../api/history/indexAPI'

class UserModel
{
    constructor(token)
    {
        this.token = token

        this.data = new UserDataModel(this.token)
    }

    get sessionID()
    {
        if(!this._sessionIDPromise)
        {
            this._sessionIDPromise = new Promise(async (resolve, reject) => {
                try {
                    if(this._sessionID)
                        return resolve(this._sessionID)
    
                    let cache = new Cache(`vitrineApp.user.${this.token}.sessionID`)
                    await cache.setCookieDriver()
        
                    let sessionID = cache.getCache()
    
                    let data = {
                        sessionID: sessionID
                    }
            
                    let vitrineUser = window.vitrineMeta.user
                    if(vitrineUser)
                        data.user = vitrineUser
        
                    let histAPI = new HistoryAPI(this.token, false)
                    let response = await histAPI.update(data)

                    if(response.data.success == false)
                        reject(`Loader-dependences: 'SessionID response invalid'`)
    
                    sessionID = response.data.data.sessionID
    
                    await this.setSessionID(sessionID)
                    this._sessionID = sessionID
                    resolve(this._sessionID)
                    
                } catch (error) {
                    reject(error)
                }
            })
        }

        return this._sessionIDPromise
    }

    async setSessionID(value)
    {
        this._sessionID = value

        let cache = new Cache(`vitrineApp.user.${this.token}.sessionID`)
        await cache.setCookieDriver()
        cache.setCache(this._sessionID)
    }
}

export default UserModel