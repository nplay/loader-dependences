import WeatherAPI from "../../../api/history/weatherAPI"

class Weather
{
    constructor(token)
    {
        this.token = token
    }

    async get()
    {
        let weatherAPI = new WeatherAPI(this.token, true)
        let response = await weatherAPI.get()

        if(response.data.success == false)
            throw new Error(`${response.data.data.message}`)

        return response.data.data
    }
}

export default Weather