
import AbstractAPI from "../abstractAPI";

class WeatherAPI extends AbstractAPI
{
    constructor(token, useCache = false)
    {
      super(token);

      this.url = this._getPath();
      this.useCache = useCache
    }

    async get()
    {
      let ip = await vitrineApp.ip()
      let date = new Date()
      date = new Date(date.setHours(date.getHours() -3))

      return await this.request.post({
        "ip": ip,
        'gteDate': date.toISOString()
      })
    }

    _getPath()
    {
      return window.vitrineApp.SERVICE.history.weather.byIP.URL
    }
}

export default WeatherAPI;