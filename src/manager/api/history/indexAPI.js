
import AbstractAPI from "../abstractAPI";
import WeatherAPI from './weatherAPI'

class indexAPI extends AbstractAPI
{
    constructor(token, useCache = false)
    {
      super(token);

      this.url = this._getPath();
      this.useCache = useCache

      this.weather = new WeatherAPI(this.token, true)
    }

    async get(sessionID = null)
    {
      return await this.request.post({
        "sessionID": sessionID
      })
    }

    async update(data)
    {
      this.headers['X-Forwarded-For'] = await vitrineApp.ip()
      
      return await this.request.patch(data)
    }
  
    _getPath()
    {
      return window.vitrineApp.SERVICE.history.get.URL
    }
}

export default indexAPI;