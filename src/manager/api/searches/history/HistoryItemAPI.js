import AbstractAPI from "../../abstractAPI";

class HistoryItemAPI extends AbstractAPI
{
    constructor(token, useCache)
    {
      super(token);

      this.url = this._getPath();
      this.useCache = useCache == undefined ? false : useCache
    }

    async get(sessionID)
    {
      return await this.request.post({
        "sessionID": sessionID
      })
    }
  
    _getPath()
    {
      let host = window.vitrineApp.API.URL
      let searches = window.vitrineApp.API.searches
      let history = searches.history
      let item = history.item
      let path = `${searches.URL}${history.URL}${item.URL}`

      return `${host}${path}`
    }
}

export default HistoryItemAPI;