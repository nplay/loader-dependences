import AbstractAPI from "../../abstractAPI";
import ByCategorys from "./new/byCategorys";

class NewItemAPI extends AbstractAPI
{
    constructor(token)
    {
        super(token);
        this.url = this._getPath();
    }

    async get()
    {
        return await this.request.get()
    }

    get byCategorys()
    {
        return new ByCategorys(this.token, this.useCache)
    }

    _getPath()
    {
        let host = window.vitrineApp.API.URL
        let searches = window.vitrineApp.API.searches
        let item = searches.item

        let path = `${searches.URL}${item.URL}${item.new.URL}`

        return `${host}${path}`
    }
}

export default NewItemAPI;