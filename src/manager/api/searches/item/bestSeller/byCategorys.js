import AbstractAPI from "../../../abstractAPI";


class ByCategorys extends AbstractAPI
{
    constructor(token, useCache = true)
    {
        super(token);
        this.url = this._getPath();
        this.useCache = useCache;
    }

    async get(ids)
    {
        ids = ids.map(id => { return parseInt(id) })
        return await this.request.post({"ids": ids})
    }

    _getPath()
    {
        let host = window.vitrineApp.API.URL
        let searches = window.vitrineApp.API.searches
        let item = searches.item

        let path = `${searches.URL}${item.URL}${item.bestSeller.URL}${item.bestSeller.categorys.URL}`

        return `${host}${path}`
    }
}

export default ByCategorys