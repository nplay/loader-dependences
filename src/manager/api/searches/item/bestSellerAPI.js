import ByCategorys from './bestSeller/byCategorys'

class BestSellerAPI
{
    constructor(token, useCache = true)
    {
        this.token = token
        this.useCache = useCache
    }

    get byCategorys()
    {
        return new ByCategorys(this.token, this.useCache)
    }
}

export default BestSellerAPI;