import AbstractAPI from "../../abstractAPI";

class BuyToBuyAPI extends AbstractAPI
{
    constructor(token)
    {
        super(token);
        
        this.url = this._getPath();
    }

    async get(itemID)
    {
        return await this.request.post({
            "ecommerce": {
                "impressions": [ { "id": itemID } ]
            }
        })
    }
  
    _getPath()
    {
        let host = window.vitrineApp.API.URL
        let searches = window.vitrineApp.API.searches
        let item = searches.item
        let buyToBuy = item.buyToBuy

        let path = `${searches.URL}${item.URL}${buyToBuy.URL}`

        return `${host}${path}`
    }
}

export default BuyToBuyAPI;