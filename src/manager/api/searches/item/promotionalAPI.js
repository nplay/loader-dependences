import AbstractAPI from "../../abstractAPI";

class PromotionalItemAPI extends AbstractAPI
{
    constructor(token)
    {
        super(token);
        this.url = this._getPath();
    }

    get byCategorys()
    {
        return new ByCategorys(this.token, this.useCache)
    }

    _getPath()
    {
        return ''
    }
}

export default PromotionalItemAPI;