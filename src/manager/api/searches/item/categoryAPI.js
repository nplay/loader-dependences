import AbstractAPI from "../../abstractAPI";

class CategoryAPI extends AbstractAPI
{
    constructor(token)
    {
        super(token);
        this.url = this._getPath();
    }

    async get(params)
    {
        return await this.request.post(params)
    }

    _getPath()
    {
        let host = window.vitrineApp.API.URL
        let searches = window.vitrineApp.API.searches
        let item = searches.item

        let path = `${searches.URL}${item.URL}${item.category.URL}`

        return `${host}${path}`
    }
}

export default CategoryAPI;