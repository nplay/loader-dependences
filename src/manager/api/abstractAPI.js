import RequestHelper from "../../helper/Http/Request";

class AbstractAPI
{
    constructor(token)
    {
        this.token = token
        this.url = null;

        this.headers = {
            'Authorization-token': this.token
        }
        
        this.useCache = false;
    }

    get request()
    {
        return new RequestHelper(this.url, this.headers, this.useCache);
    }
}

export default AbstractAPI