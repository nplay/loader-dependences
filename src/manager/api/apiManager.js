
import PopupAPI from './themes/popupAPI'
import BuyToBuyAPI from './searches/item/buyToBuyAPI';
import HistoryItemAPI from "./searches/history/HistoryItemAPI"
import NewItemAPI from './searches/item/newItemAPI'
import CategoryItemAPI from './searches/item/categoryAPI'
import BestSellerAPI from './searches/item/bestSellerAPI'
import PromotionalItemAPI from './searches/item/promotionalAPI'

class APIManager
{
    constructor()
    {
        this.themes = {
            get popupAPI()
            {
                return PopupAPI
            }
        }

        this.searches = {
            get buyToBuyAPI()
            {
                return BuyToBuyAPI
            },

            get BestSellerItemAPI()
            {
                return BestSellerAPI
            },

            get HistoryItemAPI()
            {
                return HistoryItemAPI
            },

            get NewItemAPI()
            {
                return NewItemAPI
            },

            get PromotionalItemAPI()
            {
                return PromotionalItemAPI
            },

            get CategoryItemAPI()
            {
                return CategoryItemAPI
            }
        }
    }
}

export default APIManager