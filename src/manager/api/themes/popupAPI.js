import AbstractAPI from "../abstractAPI";

class PopupAPI extends AbstractAPI
{
    constructor(token, useCache)
    {
        super(token);
        this.url = this._getPath();
        this.useCache = useCache == undefined ? false : useCache
    }

    async get(body)
    {
        body = Object.assign({"filters": { "enabled": true }, "attributes": {}}, body)
        return await this.request.post(body)
    }

    _getPath()
    {
        let host = window.vitrineApp.API.URL
        let themes = window.vitrineApp.API.themes
        let popup = themes.popup

        let path = `${themes.URL}${popup.URL}`

        return `${host}${path}`
    }
}

export default PopupAPI;