import AbstractAPI from "../abstractAPI";

class IndexAPI extends AbstractAPI
{
    constructor(token, useCache)
    {
        super(token);
        
        this.url = this._getPath();
        this.useCache = useCache == undefined ? false : useCache
    }

    async get()
    {
        try{
            var origUrl = this.url
            
            let _url = `${this.url}${token}`
            this.url = _url
            let response = await this.request.get()
            return response.data.data
        }
        finally {
            this.url = origUrl
        }
    }

    _getPath()
    {
        let host = window.vitrineApp.API.URL
        let user = window.vitrineApp.API.user

        let path = `${user.URL}`
        return `${host}${path}`
    }
}

export default IndexAPI;