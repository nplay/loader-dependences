import LibraryManager from "./library/libraryManager"
import UserModel from "./client/model/user"

class ClientModel
{
    constructor(token)
    {
        this.info = {version: '1.0.0'} //By token data

        this.token = token

        this.user = new UserModel(this.token)

        this.publicLibraries = {}
    }

    get library()
    {
        if(!this._library)
            this._librarys = new LibraryManager(this.token)
        
        return this._librarys
    }

    get CDN_URL_VITRINE()
    {
        let url = `${window.vitrineApp.CDN.URL}${window.vitrineApp.CDN.URL_VITRINE}${this.info.version}/`
        return url
    }
}

export default ClientModel