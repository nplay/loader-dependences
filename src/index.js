
import Manager from './manager'

/**
*   INITIALIZE ALL LIBRARIES
*/
try {
    let manager = new Manager();
    manager.initialize()
} catch (error) {
    console.error('Fail in load library', error)
}