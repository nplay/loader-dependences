
import Abstract from './abstract'

class Cookie extends Abstract
{
   constructor(key)
   {
      super(key)

      this._sufixCookie = this.key
   }

   hasCached()
   {
      return this._getSession(this.key) != null
   }

   getCache()
   {
      return this.getCookie(this.key)
   }
   
   setCache(value)
   {
      this.setCookie(value)
   }

   setCookie(cvalue, exdays)
   {
     exdays = exdays == undefined ? 30 : exdays
 
     var d = new Date();
     d.setTime(d.getTime() + (exdays*24*60*60*1000));
     var expires = "expires="+ d.toUTCString();
     document.cookie = this._sufixCookie + "=" + cvalue + ";" + expires + ";path=/";
   }
 
   getCookie(cname)
   {
      var name = cname + "=";
      var decodedCookie = decodeURIComponent(document.cookie);
      var ca = decodedCookie.split(';');
      for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
          c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
          return c.substring(name.length, c.length);
        }
      }

      return null
   }
}

export default Cookie;