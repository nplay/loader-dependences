
class Abstract
{
   constructor(key)
   {
      this.key = key
   }

   hasCached()
   {
   }

   getCache()
   {
   }

   setCache(value)
   {
   }
}

export default Abstract;