
import Abstract from './abstract'

class Session extends Abstract
{
   constructor(key)
   {
      super(key)
   }

   hasCached()
   {
      return this._getSession(this.key) != null
   }

   getCache()
   {
      return this._getSession(this.key)
   }

   setCache(value)
   {
      this._setSession(this.key, value)
   }

   _getSession(key)
   {
      return sessionStorage.getItem(key);
   }

   _setSession(key, value)
   {
      return sessionStorage.setItem(key, value)
   }
}

export default Session;