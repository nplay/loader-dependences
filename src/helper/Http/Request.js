
import axios from 'axios'
import Cache from '../cache'

class RequestHelper
{
    constructor(url, headers = {}, useCache)
    {
        this.config = {
            url: url,
            headers: Object.assign({
                'Authorization-token': null
            }, headers)
        }

        this.useCache = useCache == undefined ? false : useCache
    }

    genHash(url, requestData)
    {
        let obj = Object.assign({url: url}, requestData)
        obj = encodeURIComponent(JSON.stringify(obj))
        let base64 = window.btoa(obj)
        
        return base64
    }

    async get(params)
    {
        let requestConfig = Object.assign(this.config, {
            params: params
        })

        return this._request(requestConfig)
    }

    /**
     * @returns {Object}
     */
    static requests()
    {
        if(!RequestHelper._hashes)
            RequestHelper._hashes = new Object

        return RequestHelper._hashes
    }

    /**
     * @param {String} hash
     * @returns {Promise}
     */
    static request(hash)
    {
        let requests = RequestHelper.requests()
        return requests[hash] || null
    }

    /**
     * 
     * @param {String} hash 
     * @param {Promise} promise
     * @returns {Promise}
     */
    static setRequest(hash, promise)
    {
        let requests = RequestHelper.requests()
        requests[hash] = promise

        return requests[hash]
    }

    static removeRequest(hash)
    {
        let requests = RequestHelper.requests()
        delete requests[hash]
    }

    async _request(extendedConfig)
    {
        let requestConfig = Object.assign(this.config, extendedConfig)
        let hash = this.genHash(this.config.url, requestConfig)

        let cacheAPI = new Cache(hash)
        let data = !this.useCache ? null : cacheAPI.getCache()
        if(data != null)
            return JSON.parse(data);

        let response = null

        //Prevent many requests
        let requestPromise = RequestHelper.request(hash)

        if(!requestPromise)
        {
            let request = axios.request(this.config.url, requestConfig)
            requestPromise = RequestHelper.setRequest(hash, request)
        }

        response = await requestPromise
        RequestHelper.removeRequest(hash)

        if(this.useCache)
            cacheAPI.setCache(JSON.stringify(response))

        return response
    }

    async post(body)
    {
        let requestConfig = Object.assign(this.config, {
            data: body,
            method: 'POST'
        })

        return this._request(this.url, requestConfig)
    }

    async patch(body)
    {
        let requestConfig = Object.assign(this.config, {
            data: body,
            method: 'PATCH'
        })

        return this._request(this.url, requestConfig)
    }
}

export default RequestHelper