
import Session from './cache/session'
import Cookie from './cache/cookie'

class Cache
{
   constructor(key)
   {
      this.key = key
   }

   get driver()
   {
      if(!this._driver)
         this._driver = new Session(this.key)

      return this._driver
   }

   set driver(instance)
   {
      this._driver = instance
      return this
   }

   async setCookieDriver()
   {
      this.driver = new Cookie(this.key)
      return this
   }

   hasCached()
   {
      return this.driver.getCache() != null
   }

   getCache()
   {
      return this.driver.getCache()
   }

   setCache(value)
   {
      return this.driver.setCache(value)
   }
}

export default Cache;