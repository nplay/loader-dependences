
class DocumentCollection extends Object
{
    constructor(classInstance)
    {
        super();
        this._classInstance = classInstance
    }

    _checkInstance(document)
    {
        let isInstance = this._classInstance == undefined || document instanceof this._classInstance

        if(!isInstance)
            throw new Error(`Collection require document instanceof: ${this._classInstance}`)
    }

    get(key)
    {
        return this[key]
    }

    exists(key)
    {
        return this.hasOwnProperty(key)
    }

    push(key, document)
    {
        if(this.exists(key))
            throw new Error(`Key ${key} already exists`)

        this._checkInstance(document)

        this[key] = document
    }

    update(key, document)
    {
        if(this.exists(key) == false)
            throw new Error(`Key ${key} not exists`)

        this._checkInstance(document)

        this[key] = document
    }
}

export default DocumentCollection