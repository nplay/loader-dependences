import VitrineAppModel from "./manager/vitrineAppModel";

class Manager
{
    constructor()
    {
    }

    initialize()
    {
        window.vitrineApp = window.vitrineApp || new VitrineAppModel()
        window.vitrineApp.clients.loadClient(this.token, this.options)
    }

    get options()
    {
        if(window.vitrineLoader)
            return window.vitrineLoader.options || {}
        
        return {}
    }

    get token()
    {
        if(!this._token)
        {
            let curSrc = document.currentScript
            let srcAttr = curSrc.src
            let srcArr = srcAttr.split('?')[1]
    
            let token = srcArr.split('=')[1]
            curSrc.setAttribute('data-token', token)
            this._token = token
        }

        return this._token
    }

    _getUrlCDNClient()
    {
        let version = this._getClientVersion()

        let urlCDN = window.vitrineApp.CDN.URL
        let urlVitrine = window.vitrineApp.CDN.URL_VITRINE

        return `${urlCDN}${urlVitrine}${version}/`
    }
}

export default Manager